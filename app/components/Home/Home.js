// @flow
import React, { Component } from 'react';
import { Input, Button } from 'antd';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import { promisify } from 'util';
import fs from 'fs';
import path from 'path';
import { PACKS_PATH, THEMES } from '../../constants';
import styles from './Home.scss';
import News from './components/News/News';
import Card from '../Common/Card/Card';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);
    this.state = {
      latestBtnClicked: false,
      latestInstalled: false
    };
  }
  /* eslint-disable */
  openLink(url) {
    require('electron').shell.openExternal(url);
  }

  componentDidMount = async () => {
    this.props.getNews();
    try {
      await promisify(fs.access)(path.join(PACKS_PATH, '1.13.2'));
      this.setState({ latestInstalled: true });
    } catch (e) {
      this.setState({ latestInstalled: false });
    }
    // Downloads the versions list just the first time
    if (this.props.versionsManifest.length === 0) {
      this.props.getVanillaMCVersions();
    }
  };

  /* eslint-enable */

  render() {
    return (
      <div>
        <main className={styles.content}>
          <div className={styles.innerContent}>
            <News news={this.props.news} />
            <div className={styles.cards}>
              <Card
                style={{
                  height: 'auto',
                  width: '100%',
                  minWidth: 420,
                  display: 'block',
                  marginTop: 15,
                  textAlign: 'center'
                }}
                title={`Welcome the Westgate Launcher ${this.props.username}`}
              >
                <div className={styles.firstCard}>
                  <div>
                    <span className={styles.titleHeader}>
                      WestgateXL is now on{' '}
                      <a
                        href="https://www.buymeacoffee.com/WestgateXL" target="_blank"
                        className={styles.patreonText}
                      >
                        BuyMeCoffee
                      </a>
                    </span>
                    <div className={styles.patreonContent}>
                      If you like the WestgateXL Launcher and you would like it to have even
                      more features and bug fixes, please shove more coffe in our mounts to consider helping us out
                      supporting all of our projects. Happy Building!
                    </div>
                  </div>
                  <div>
                    Your able to chill with us on one of these links:
                    <div className={styles.discord}>
                      <a href="https://discord.io/Westgate" target="_blank">Discord</a>
                    </div>
                    <div className={styles.github}>
                      <a href="https://gitlab.com/WestgateXL" target="_blank">Project Lab</a>
                    </div>
                    <div className={styles.twitter}>
                      <a href="https://twitter.com/WestgateXL" target="_blank">Twitter</a>
                    </div>
                    <div className={styles.facebook}>
                      <a href="https://facebook.com/westgatestudio/?ref=bookmarks" target="_blank">Facebook</a>
                    </div>
                    <div className={styles.facebook}>
                      <a href="https://m.me/westgatestudio" target="_blank">Message Us</a>
                    </div>
                  </div>
                </div>
              </Card>
              <Card
                style={{
                  height: 170,
                  width: '100%',
                  minWidth: 420,
                  display: 'block',
                  marginTop: 15,
                  textAlign: 'center'
                }}
                title="Try out the new v1.13.2"
              >
                V1.13.2 has just been released. Wanna try it out?
                {this.state.latestBtnClicked || this.state.latestInstalled ? (
                  <Link
                    to="/dmanager"
                    style={{ display: 'block', margin: '35px auto' }}
                  >
                    Go to your instances
                  </Link>
                ) : (
                  <Button
                    type="primary"
                    loading={this.props.packCreationLoading}
                    style={{ display: 'block', margin: '35px auto' }}
                    onClick={() => {
                      this.props.createPack('1.13.2', '1.13.2');
                      this.setState({ latestBtnClicked: true });
                    }}
                  >
                    Install Minecraft's Latest Version
                  </Button>
                )}
              </Card>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
