import React, { Component } from 'react';

type Props = {};

class Worlds extends Component<Props> {
  props: Props;

  render() {
    return (
      <div>
        <h1>Coming Soon</h1>
        <p>Please refer to <a href="https://gitlab.com/WestgateXL/westgateui" rel="noopener noreferrer" target="_blank"><span style="color: rgb(226, 80, 65);">GitLabs</span></a> for updates</p>
      </div>
    );
  }
}
export default Worlds;
