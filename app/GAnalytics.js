import ua from 'universal-analytics';

const ga = uuid => ua('ca-pub-2948090993116906', uuid);

export default ga;
